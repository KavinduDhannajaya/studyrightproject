

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:studyRight/screens/chat_UI.dart';
import 'package:studyRight/screens/details.dart';
import 'package:studyRight/screens/home.dart';
import 'package:studyRight/screens/signup.dart';

// ignore: camel_case_types
class mynavBar extends StatefulWidget {
  @override
  _mynavBarState createState() => _mynavBarState();
}

// ignore: camel_case_types
class _mynavBarState extends State<mynavBar> {
  int _currentIndex=0;
  final children= [
    Home(),
    SignUpPage(),
    ChatScreen(),
    Details(),

  ];
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type:BottomNavigationBarType.fixed,
        backgroundColor: Colors.black26,


        items:
        [
          BottomNavigationBarItem
            (
              icon: new Icon(Icons.home),
            title: new Text("Home"),
            backgroundColor: Colors.black26
          ),

          BottomNavigationBarItem
            (
              icon: new Icon(Icons.favorite),
              title: new Text("Bookmarked"),
              backgroundColor: Colors.black26
          ),
          BottomNavigationBarItem
            (
              icon: new Icon(Icons.chat_bubble),
              title: new Text("Assistant"),
              backgroundColor: Colors.black26
          ),
          BottomNavigationBarItem
            (
              icon: new Icon(Icons.person),
              title: new Text("profile"),
              backgroundColor: Colors.black26
          )
        ],
        onTap: (index) {
          setState(() {
            _currentIndex=index;
          });
        },
      ) ,

    );
  }
}
